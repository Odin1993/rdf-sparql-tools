<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="quickStart&#xa;RDF-SPARQL" FOLDED="false" ID="ID_540758224" CREATED="1588796027095" MODIFIED="1589147872733" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12" RULE="ON_BRANCH_CREATION"/>
<node TEXT="generate RDF" POSITION="right" ID="ID_548049507" CREATED="1588796104393" MODIFIED="1588796110249">
<edge COLOR="#ff0000"/>
<node TEXT="manually" ID="ID_874561483" CREATED="1588796111914" MODIFIED="1588796117254">
<node TEXT="any text editor" ID="ID_1128321836" CREATED="1589147913917" MODIFIED="1589147921859"/>
<node TEXT="pluma, atom, notepad++ have nice syntax highlighting" ID="ID_1107589765" CREATED="1589147923100" MODIFIED="1589147949391"/>
</node>
<node TEXT="programmatically" ID="ID_986970174" CREATED="1588796119865" MODIFIED="1588796126892">
<node TEXT="cf. python rdflib" ID="ID_958005226" CREATED="1589147960388" MODIFIED="1589147967113"/>
<node TEXT="from tabulated files" ID="ID_1633621772" CREATED="1633674907019" MODIFIED="1633674920642">
<node TEXT="tarql" ID="ID_1208756193" CREATED="1633674921886" MODIFIED="1633674963308">
<node TEXT="https://tarql.github.io/" ID="ID_865629222" CREATED="1633674965566" MODIFIED="1633674992564">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
<node TEXT="convert to named graph" ID="ID_876330368" CREATED="1633675072030" MODIFIED="1633675080167">
<node TEXT="https://gitlab.com/odameron/rdf2namedGraph" ID="ID_652275586" CREATED="1633675092773" MODIFIED="1633675106858">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="generate VoID description" ID="ID_1691885962" CREATED="1633676268526" MODIFIED="1633676276309">
<node TEXT="https://gitlab.com/odameron/VoIDmetricsGenerator" ID="ID_783862772" CREATED="1633676285897" MODIFIED="1633676304812">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="store RDF" POSITION="right" ID="ID_835941119" CREATED="1588796136673" MODIFIED="1588796146683">
<edge COLOR="#0000ff"/>
<node TEXT="in file(s)" ID="ID_634449324" CREATED="1588796146691" MODIFIED="1588796155004"/>
<node TEXT="on disk for jena" ID="ID_1365750706" CREATED="1588796169113" MODIFIED="1588796195198">
<node TEXT="generates" ID="ID_1596058328" CREATED="1589147989428" MODIFIED="1589242423358">
<node TEXT="a TDB directory with the data" ID="ID_612906183" CREATED="1589242424581" MODIFIED="1589242462504"/>
<node TEXT="the internal indexes required for improving performances" ID="ID_1467086981" CREATED="1589242427549" MODIFIED="1589242455001"/>
</node>
<node TEXT="${JENA_HOME}/bin/tdbloader2 --loc=/path/to/TDBdirectory file [files]" ID="ID_249229024" CREATED="1589241891408" MODIFIED="1649895250495">
<icon BUILTIN="executable"/>
<font NAME="Courier" STRIKETHROUGH="true"/>
</node>
<node TEXT="${JENA_HOME}/bin/tdb1.xloader --loc=/path/to/TDBdirectory file [files]" ID="ID_1703845047" CREATED="1589241891408" MODIFIED="1649895231805">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="in a triplestore" ID="ID_1599891421" CREATED="1589242922669" MODIFIED="1589243034440">
<node TEXT="when you need to integrate and/or query datasets" ID="ID_69951950" CREATED="1589243034451" MODIFIED="1589248045339">
<icon BUILTIN="idea"/>
</node>
<node TEXT="stores data in an internal representation, usually with its own indexes for improving performances" ID="ID_1221821522" CREATED="1589243084895" MODIFIED="1589243121741"/>
<node TEXT="the overhead of the indexes is justified with medium to large datasets" ID="ID_860350479" CREATED="1588796581281" MODIFIED="1588796604133"/>
<node TEXT="exposes the data for querying via a SPARQL endpoint" ID="ID_643042154" CREATED="1589243122142" MODIFIED="1589243180286"/>
<node TEXT="usually provides a Web interface for users to" ID="ID_1580252046" CREATED="1589243180765" MODIFIED="1589243225648">
<node TEXT="write their query" ID="ID_179692030" CREATED="1589243225658" MODIFIED="1589243236876"/>
<node TEXT="display/download the results" ID="ID_638190105" CREATED="1589243237310" MODIFIED="1589243370593"/>
</node>
</node>
</node>
<node TEXT="process RDF" POSITION="right" ID="ID_59881371" CREATED="1634749768470" MODIFIED="1634749772035">
<edge COLOR="#ff0000"/>
<node TEXT="validate RDF" ID="ID_1362909083" CREATED="1589049305380" MODIFIED="1634749774884">
<node TEXT="requires jena" ID="ID_44784779" CREATED="1589145235619" MODIFIED="1589145239139"/>
<node TEXT="${JENA_HOME}/bin/riot --validate &lt;filePath&gt;" ID="ID_90140608" CREATED="1589145093634" MODIFIED="1589242071180">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="convert between file formats" ID="ID_956849993" CREATED="1589049326660" MODIFIED="1634749777768">
<node TEXT="requires Raptor RDF library" ID="ID_1450234423" CREATED="1589145287042" MODIFIED="1589145395551"/>
<node TEXT="rapper -i &lt;inputFormat&gt; -o &lt;outputFormat&gt; inputFile &gt; outputFile" ID="ID_1449752137" CREATED="1589146648188" MODIFIED="1589242122664">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="data schema" ID="ID_25317239" CREATED="1633675284038" MODIFIED="1634749780487">
<node TEXT="generate" ID="ID_1291024650" CREATED="1633675977975" MODIFIED="1633675980797">
<node TEXT="https://github.com/askomics/abstractor" ID="ID_1101990001" CREATED="1633675351766" MODIFIED="1633675364611">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="visualize" ID="ID_1094953124" CREATED="1633675981047" MODIFIED="1633675983916">
<node TEXT="https://gitlab.com/odameron/ontologySchema2image" ID="ID_734885898" CREATED="1633676015287" MODIFIED="1633676027087">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="draw diagram from (small) RDF file" ID="ID_536435154" CREATED="1598541050090" MODIFIED="1634749782425">
<node TEXT="rapper --input turtle ${rdfFilePath} --output dot | dot -Tpng -o ${pngFilePath}" ID="ID_1100742866" CREATED="1598541068444" MODIFIED="1598541150511">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
<node TEXT="nicer interface" ID="ID_1337409096" CREATED="1598541096381" MODIFIED="1598541164697">
<node TEXT="https://gitlab.com/odameron/rdf2image" ID="ID_980832144" CREATED="1598541165893" MODIFIED="1598541191985">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="https://spex.zazuko.com/#/rdf-editor" ID="ID_875981514" CREATED="1625072892631" MODIFIED="1625072904000">
<icon BUILTIN="internet"/>
</node>
<node TEXT="ontology" ID="ID_1245490609" CREATED="1633676071609" MODIFIED="1633676145726">
<node TEXT="hierarchy" ID="ID_1228828868" CREATED="1633676129231" MODIFIED="1633676132550">
<node TEXT="https://gitlab.com/odameron/ontologyHierarchyVisualization" ID="ID_1805193693" CREATED="1633676081432" MODIFIED="1633676103479">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="schema" ID="ID_1991309017" CREATED="1633676132904" MODIFIED="1633676134941">
<node TEXT="https://gitlab.com/odameron/ontologySchema2image" ID="ID_1923698173" CREATED="1633676015287" MODIFIED="1633676027087">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
<node TEXT="snowman: static HTML site generator from SPARQL backend" ID="ID_161736680" CREATED="1634749904408" MODIFIED="1634749926839">
<node TEXT="https://github.com/glaciers-in-archives/snowman/" ID="ID_1676836401" CREATED="1634749949241" MODIFIED="1634749966116">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="query RDF with SPARQL" POSITION="right" ID="ID_1855035382" CREATED="1588796213017" MODIFIED="1588796222510">
<edge COLOR="#00ff00"/>
<node TEXT="apache jena" ID="ID_1326283710" CREATED="1588796254440" MODIFIED="1588796317822">
<node TEXT="query local data" ID="ID_577677466" CREATED="1589243371758" MODIFIED="1589243376262">
<node TEXT="the data is loaded into RAM before each query" ID="ID_1345738660" CREATED="1588796298193" MODIFIED="1588796340634"/>
<node TEXT="well adapted for one-shot queries over a small dataset via the command line" ID="ID_934355386" CREATED="1588796340993" MODIFIED="1589243519852">
<icon BUILTIN="idea"/>
</node>
<node TEXT="${JENA_HOME}/bin/sparql --data=/path/to/dataFile --query=/path/to/queryFile" ID="ID_437907219" CREATED="1589242140662" MODIFIED="1589242319553">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
<node TEXT="use as many &quot;--data=file&quot; as necessary" ID="ID_233895734" CREATED="1589242289621" MODIFIED="1589242316068"/>
<node TEXT="there is a &quot;--explain&quot; option (explain and log query execution)" ID="ID_244432442" CREATED="1589242842798" MODIFIED="1589242876050"/>
</node>
<node TEXT="query a remote endpoint" ID="ID_1941816986" CREATED="1589243377503" MODIFIED="1589243383996">
<node TEXT="well adapted for sending a query to a remote endpoint (and retrieving the result) via the command line" ID="ID_740565069" CREATED="1589243435838" MODIFIED="1589243503635">
<icon BUILTIN="idea"/>
</node>
<node TEXT="${JENA_HOME}/bin/rsparql --service=endpointURL --query=/path/to/queryFile" ID="ID_1734226806" CREATED="1589243525814" MODIFIED="1589243595967">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="setup a SPARQL endpoint" ID="ID_164795908" CREATED="1588796446609" MODIFIED="1588796454815">
<node TEXT="SPARQL endpoint =" ID="ID_550202133" CREATED="1588796458049" MODIFIED="1588796524106">
<node TEXT="provided by a triplestore for exposing its data" ID="ID_628677778" CREATED="1589243677703" MODIFIED="1589243728120">
<node TEXT="on your machine" ID="ID_867814176" CREATED="1589243774863" MODIFIED="1589243831933"/>
<node TEXT="on a remote server" ID="ID_73926345" CREATED="1589243832871" MODIFIED="1589243838478"/>
</node>
<node TEXT="takes SPARQL queries from the user as input" ID="ID_598966506" CREATED="1588796613640" MODIFIED="1589243739207"/>
<node TEXT="processes and returns the result" ID="ID_1808867030" CREATED="1588796632273" MODIFIED="1589243856072"/>
</node>
<node TEXT="well adapted for multiple queries over medium to large datasets" ID="ID_1292685582" CREATED="1588797044785" MODIFIED="1588797521791">
<icon BUILTIN="idea"/>
</node>
<node TEXT="apache fuseki" ID="ID_349694919" CREATED="1588797126625" MODIFIED="1588797134414">
<node TEXT="easy to setup" ID="ID_966440060" CREATED="1588797182633" MODIFIED="1589248228633">
<icon BUILTIN="idea"/>
</node>
<node TEXT="if necessary (hint: almost never), increase heap space" ID="ID_1074326261" CREATED="1661327063679" MODIFIED="1661327093843">
<node TEXT="edit ${FUSEKI_HOME}/fuseki-server" ID="ID_1327112388" CREATED="1661327097168" MODIFIED="1661327146521"/>
<node TEXT="#JVM_ARGS=${JVM_ARGS:--Xmx4G}&#xa;JVM_ARGS=${JVM_ARGS:--Xmx16G}" ID="ID_457698153" CREATED="1661327147175" MODIFIED="1661327202524">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="for medium to large datasets and simple to medium queries" ID="ID_447346437" CREATED="1588797193433" MODIFIED="1589248226015">
<icon BUILTIN="idea"/>
</node>
<node TEXT="setup = 2 modes" ID="ID_730018637" CREATED="1588797275337" MODIFIED="1589248303345">
<node TEXT="fuseki in RAM" ID="ID_914965848" CREATED="1588796379536" MODIFIED="1588796384620">
<node TEXT="converts the data into its internal representation + generates the indexes when starting" ID="ID_301977438" CREATED="1589244240159" MODIFIED="1589244282923"/>
<node TEXT="well adapted for" ID="ID_331222928" CREATED="1589244283262" MODIFIED="1589244316828">
<icon BUILTIN="idea"/>
<node TEXT="small to medium datasets (that fit in RAM)" ID="ID_1953311721" CREATED="1589244310150" MODIFIED="1589244335782"/>
<node TEXT="which you may not need to reuse or which are small enough for the delay to remain acceptable" ID="ID_626361407" CREATED="1589244336295" MODIFIED="1589244445259"/>
</node>
<node TEXT="${FUSEKI_HOME}/fuseki-server --file=/path/to/RDFfile /serviceName" ID="ID_1448627526" CREATED="1589248132867" MODIFIED="1660922927196">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
<node TEXT="do not close the terminal!" ID="ID_733050077" CREATED="1660923409844" MODIFIED="1660923430750">
<icon BUILTIN="stop-sign"/>
</node>
<node TEXT="&quot;serviceName&quot; is the (short) name of your dataset" ID="ID_636760640" CREATED="1589248182467" MODIFIED="1589248425603"/>
<node TEXT="use as many &quot;--file=&quot; as necessary" ID="ID_1308969919" CREATED="1660922055305" MODIFIED="1660922073343"/>
<node TEXT="use &quot;--update&quot; if the dataset can be modified" ID="ID_1494529582" CREATED="1589248322715" MODIFIED="1589248346114"/>
</node>
<node TEXT="fuseki from TDB on disk" ID="ID_1187564468" CREATED="1588796386712" MODIFIED="1588796417083">
<node TEXT="the conversion of the data and generation of the internal indexes have been performed during the creation of the TDB directory" ID="ID_230610124" CREATED="1589243909830" MODIFIED="1589243996246"/>
<node TEXT="fast start" ID="ID_1795372934" CREATED="1589243997775" MODIFIED="1589244027766"/>
<node TEXT="well adapted for" ID="ID_1076624638" CREATED="1589244029134" MODIFIED="1589244038946">
<icon BUILTIN="idea"/>
<node TEXT="large datasets that may not fit in RAM" ID="ID_415889245" CREATED="1589244040943" MODIFIED="1589244072022"/>
<node TEXT="medium to large datasets that you plan to reuse several times" ID="ID_699080728" CREATED="1589244092280" MODIFIED="1660922643650">
<font NAME="SansSerif"/>
</node>
</node>
<node TEXT="${FUSEKI_HOME}/fuseki-server --loc=/path/to/TDBdirectory /serviceName" ID="ID_391690029" CREATED="1589244127158" MODIFIED="1589248434898">
<icon BUILTIN="executable"/>
<font NAME="Courier"/>
</node>
<node TEXT="do not close the terminal!" ID="ID_1962652609" CREATED="1660923409844" MODIFIED="1660923430750">
<icon BUILTIN="stop-sign"/>
</node>
<node TEXT="&quot;serviceName&quot; is the (short) name of your dataset" ID="ID_1936628986" CREATED="1589244203559" MODIFIED="1589248439944"/>
<node TEXT="use &quot;--update&quot; if the dataset can be modified" ID="ID_1428889764" CREATED="1589248322715" MODIFIED="1589248346114"/>
</node>
</node>
<node TEXT="query" ID="ID_390322234" CREATED="1589248532604" MODIFIED="1589248535579">
<node TEXT="users: web interface = http://localhost:3030" ID="ID_1257419674" CREATED="1589248551275" MODIFIED="1589248579008"/>
<node TEXT="programs: endpoint URL" ID="ID_894199623" CREATED="1589248445572" MODIFIED="1589248550826">
<node TEXT="http://localhost:3030/serviceName/query" ID="ID_692182413" CREATED="1589248459355" MODIFIED="1630145675132">
<font NAME="Courier"/>
</node>
<node TEXT="http://localhost:3030/serviceName/update" ID="ID_765404072" CREATED="1589248482628" MODIFIED="1630145684873">
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
<node TEXT="openlinks virtuoso" ID="ID_1703241600" CREATED="1588797135897" MODIFIED="1588797142518">
<node TEXT="slightly more difficult to setup" ID="ID_969755331" CREATED="1588797149257" MODIFIED="1588797167350"/>
<node TEXT="for large datasets and/or complex queries" ID="ID_741565568" CREATED="1588797169073" MODIFIED="1589248282111">
<icon BUILTIN="idea"/>
</node>
<node TEXT="query" ID="ID_133786757" CREATED="1630145382299" MODIFIED="1630145384869">
<node TEXT="users: web interface = http://localhost:8890/sparql" ID="ID_68696444" CREATED="1630145413466" MODIFIED="1630145427261"/>
<node TEXT="programs" ID="ID_1418211024" CREATED="1630145434226" MODIFIED="1630145478368">
<node TEXT="virtuoso tool" ID="ID_544614075" CREATED="1630145635522" MODIFIED="1630145646689">
<node TEXT="${VIRTUOSO_HOME}/bin/isql localhost dba dba /path/to/queryFile" ID="ID_1175969556" CREATED="1630145556450" MODIFIED="1630145714226">
<font NAME="Courier"/>
</node>
<node TEXT="queryFile must start with the string &quot;SPARQL&quot; (without quotes)" ID="ID_1038629756" CREATED="1630145715521" MODIFIED="1630145749404">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="generic sparql" ID="ID_197059395" CREATED="1630145647033" MODIFIED="1630145650955">
<node TEXT="http://localhost:8890/sparql" ID="ID_310151493" CREATED="1630146292178" MODIFIED="1630146301890">
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="tools" POSITION="right" ID="ID_1397557291" CREATED="1589145397946" MODIFIED="1589145401001">
<edge COLOR="#00007c"/>
<node TEXT="apache jena" ID="ID_444025141" CREATED="1589145402609" MODIFIED="1589145410624">
<node TEXT="https://jena.apache.org/" ID="ID_66290494" CREATED="1589147651116" MODIFIED="1589147660868">
<icon BUILTIN="internet"/>
</node>
<node TEXT="extract in an easy location" ID="ID_1063183218" CREATED="1589147733348" MODIFIED="1589147750705"/>
<node TEXT="(optional) export JENA_HOME=/..." ID="ID_883241211" CREATED="1589147751116" MODIFIED="1589147851557">
<icon BUILTIN="executable"/>
</node>
<node TEXT="Hidden gems included with Jena&#x2019;s command line utilities" ID="ID_18389127" CREATED="1636324831658" MODIFIED="1636324871042">
<icon BUILTIN="idea"/>
<node TEXT="https://www.bobdc.com/blog/jenagems/" ID="ID_875846592" CREATED="1636324849938" MODIFIED="1636324863214">
<icon BUILTIN="internet"/>
</node>
<node TEXT="rdfdiff" ID="ID_1259625294" CREATED="1636324880426" MODIFIED="1636324883408"/>
<node TEXT="working with fuseki datasets from the command line" ID="ID_90804734" CREATED="1636324907818" MODIFIED="1636324919232"/>
<node TEXT="riot" ID="ID_1003902778" CREATED="1636324919697" MODIFIED="1636324921393"/>
</node>
</node>
<node TEXT="apache fuseki" ID="ID_1690685535" CREATED="1589145410993" MODIFIED="1589145422312">
<node TEXT="https://jena.apache.org/documentation/fuseki2/index.html" ID="ID_1498174" CREATED="1589147691493" MODIFIED="1589147698549">
<icon BUILTIN="internet"/>
</node>
<node TEXT="extract in a easy location" ID="ID_1921514092" CREATED="1589147822211" MODIFIED="1589147830650"/>
<node TEXT="(optional) export FUSEKI_HOME=/..." ID="ID_1242489635" CREATED="1589147831260" MODIFIED="1589147859959">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="Raptor RDF library" ID="ID_217643215" CREATED="1589145423033" MODIFIED="1589145434024">
<node TEXT="command line tools for manipulating RDF" ID="ID_475396338" CREATED="1589145435345" MODIFIED="1589145447824"/>
<node TEXT="debian: package raptor2-utils" ID="ID_594780769" CREATED="1589145491346" MODIFIED="1589145503886"/>
</node>
<node TEXT="python rdflib" ID="ID_496173938" CREATED="1589146744917" MODIFIED="1589146755174">
<node TEXT="python module for working with RDF" ID="ID_647261715" CREATED="1589146756675" MODIFIED="1589146787546"/>
<node TEXT="read" ID="ID_371765008" CREATED="1589146788211" MODIFIED="1589146789705"/>
<node TEXT="write" ID="ID_1642655013" CREATED="1589146790019" MODIFIED="1589146792800"/>
<node TEXT="query (not efficient)" ID="ID_37219444" CREATED="1589146793580" MODIFIED="1589146804682"/>
</node>
<node TEXT="python-sparqlwrapper" ID="ID_220659696" CREATED="1589146848844" MODIFIED="1589146856250">
<node TEXT="python module for SPARQL" ID="ID_131870011" CREATED="1589146859011" MODIFIED="1589146873770"/>
<node TEXT="send queries to an endpoint" ID="ID_1275681318" CREATED="1589146874148" MODIFIED="1589146881706"/>
<node TEXT="retrieve result as a dictionary" ID="ID_112463441" CREATED="1589146882236" MODIFIED="1589146895513">
<node TEXT="for data analysis, consider" ID="ID_789618246" CREATED="1589146952516" MODIFIED="1589146996342">
<node TEXT="sparqldataframe" ID="ID_763607071" CREATED="1589146996350" MODIFIED="1589147472597">
<icon BUILTIN="ksmiletris"/>
</node>
<node TEXT="sparql-dataframe" ID="ID_1429022433" CREATED="1589147003508" MODIFIED="1589147478140">
<icon BUILTIN="smiley-neutral"/>
</node>
</node>
</node>
</node>
<node TEXT="python + SPARQL + pandas" ID="ID_1127628111" CREATED="1589147342356" MODIFIED="1589147362353">
<node TEXT="sparqldataframe" ID="ID_65878410" CREATED="1589147366204" MODIFIED="1589147480751">
<icon BUILTIN="ksmiletris"/>
<node TEXT="https://pypi.org/project/sparqldataframe/" ID="ID_1925926533" CREATED="1589147501891" MODIFIED="1634801689223">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
<node TEXT="import sparqldataframe" ID="ID_1906597999" CREATED="1589147567491" MODIFIED="1589248687718">
<font NAME="Courier"/>
</node>
<node TEXT="endpoint = &quot;http://...&quot;" ID="ID_1754860585" CREATED="1589147574005" MODIFIED="1589248703037">
<font NAME="Courier"/>
</node>
<node TEXT="query= &quot;SELECT...&quot;" ID="ID_1175516037" CREATED="1589147606844" MODIFIED="1589248703051">
<font NAME="Courier"/>
</node>
<node TEXT="df = sparqldataframe.query(endpoint, query)" ID="ID_505630006" CREATED="1589147586100" MODIFIED="1589799019711">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="sparql-dataframe" ID="ID_1597436665" CREATED="1589147372796" MODIFIED="1589147483594">
<icon BUILTIN="smiley-neutral"/>
<node TEXT="https://github.com/lawlesst/sparql-dataframe" ID="ID_1234080207" CREATED="1589147520379" MODIFIED="1634801675388">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
<node TEXT="Documentation/resources" POSITION="right" ID="ID_1450926147" CREATED="1624830780927" MODIFIED="1624830885199">
<edge COLOR="#7c007c"/>
<node TEXT="Bob DuCharme" ID="ID_1453091549" CREATED="1624830854479" MODIFIED="1627598338531">
<node TEXT="What is RDF?" ID="ID_828859626" CREATED="1627598295625" MODIFIED="1673718751883">
<icon BUILTIN="idea"/>
<node TEXT="http://www.bobdc.com/blog/whatisrdf/" ID="ID_1771256853" CREATED="1624830842062" MODIFIED="1634801700077">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="What is RDFS?" ID="ID_1133583974" CREATED="1627598302027" MODIFIED="1673718756240">
<icon BUILTIN="idea"/>
<node TEXT="http://www.bobdc.com/blog/whatisrdfs/" ID="ID_662953337" CREATED="1627598308184" MODIFIED="1634801708511">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="What else can I do with RDFS?" ID="ID_119678709" CREATED="1634801605769" MODIFIED="1634801785747">
<node TEXT="https://www.bobdc.com/blog/whatisrdfspart2/" ID="ID_1228506763" CREATED="1634801614139" MODIFIED="1634801657158">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="You probably don&apos;t need OWL" ID="ID_1125160253" CREATED="1634801746222" MODIFIED="1634801754413">
<node TEXT="https://www.bobdc.com/blog/dontneedowl/" ID="ID_1463358005" CREATED="1634801755813" MODIFIED="1634801774034">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
<node TEXT="Irene Polikoff: Why we use OWL every day at Triply" ID="ID_1909392905" CREATED="1634802115917" MODIFIED="1634802136259">
<node TEXT="https://triply.cc/blog/2021-08-why-we-use-owl" ID="ID_1245569560" CREATED="1634802137614" MODIFIED="1634802176574">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="SPARQL in 11 minutes" ID="ID_790235785" CREATED="1624830889038" MODIFIED="1627598389886">
<node TEXT="https://www.youtube.com/watch?v=FvGndkpa4K0" ID="ID_998788910" CREATED="1624830919158" MODIFIED="1634801719178">
<icon BUILTIN="video"/>
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="SPARQL cheat sheet (sp for named graphs)" ID="ID_1590403712" CREATED="1624868633299" MODIFIED="1624868657094">
<node TEXT="https://github.com/relu91/sparqlCheatSheets" ID="ID_1282018461" CREATED="1624868658447" MODIFIED="1624868668749">
<icon BUILTIN="internet"/>
</node>
</node>
<node TEXT="Stardog videos on RDF and SPARQL" ID="ID_253923012" CREATED="1673718774701" MODIFIED="1673718788337">
<node TEXT="getting started with RDF and SPARQL" ID="ID_1827729782" CREATED="1673718845124" MODIFIED="1673719148139">
<icon BUILTIN="idea"/>
<node TEXT="https://www.youtube.com/watch?v=qG1d-i9njsg" ID="ID_225190691" CREATED="1673718857323" MODIFIED="1673718876236">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="advanced RDF and SPARQL" ID="ID_481883560" CREATED="1673718879715" MODIFIED="1673719150758">
<icon BUILTIN="idea"/>
<node TEXT="https://www.youtube.com/watch?v=vaHJqPIvnXs" ID="ID_796064309" CREATED="1673718905379" MODIFIED="1673718924459">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="https://www.youtube.com/results?search_query=Stardog+sparql" ID="ID_548462706" CREATED="1673718893275" MODIFIED="1673718983542">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
</map>
