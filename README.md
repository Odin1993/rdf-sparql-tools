# rdf-sparql-tools

Quickstart guide to the technical aspects of the basic tools related to RDF, SPARQL and OWL.

See also [https://gitlab.com/odameron/rdf-sparql-cheatsheet](https://gitlab.com/odameron/rdf-sparql-cheatsheet) for a basic cheat sheet, and [https://gitlab.com/odameron/fusekiInstallationUsage](https://gitlab.com/odameron/fusekiInstallationUsage) for step-by-step instructions for installing fuseki.
